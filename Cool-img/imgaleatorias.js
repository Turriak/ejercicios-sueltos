const fs = require('fs');
let i=0;

function logUrl(aImg) {
    for (const url of aImg) {
        fs.writeFileSync('log.txt','URL ' + (i=i+1) + ' '+ url + '\n',{flag: 'a'}); 
        }
}

function showUrl() {
    console.log(fs.readFileSync('./log.txt').toString());
}

module.exports = {
    logUrl: logUrl,
    showUrl: showUrl
};
    
 