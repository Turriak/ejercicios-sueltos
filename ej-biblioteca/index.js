const express = require('express');
const app = express();
const { listaAutores, crearAutores, listaAutoresId, eliminarAutoreId, modificaAutorID,
    listaLibrosId, agregarLibrosId, librosId, modificaLibrosId, eliminarLibrosId,
    middlewareAutores, middlewareLibros } = require('./funciones');



app.use(express.json());

app.get('/autores', listaAutores);
app.post('/autores', crearAutores);

app.use('/autores/:id',middlewareAutores);

app.get('/autores/:id', listaAutoresId);
app.delete('/autores/:id', eliminarAutoreId);
app.put('/autores/:id', modificaAutorID);

app.use('/autores/:id/libros/:idLibro',middlewareLibros);

app.get('/autores/:id/libros', listaLibrosId);
app.post('/autores/:id/libros', agregarLibrosId);

app.get('/autores/:id/libros/:idLibro', librosId);
app.put('/autores/:id/libros/:idLibro', modificaLibrosId);
app.delete('/autores/:id/libros/:idLibro', eliminarLibrosId);


app.listen(8080, () => console.log('server ready'));

