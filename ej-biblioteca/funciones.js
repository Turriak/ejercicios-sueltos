const { autores } = require('./array');

// --== Middleware Autores ==--
function middlewareAutores(req, res, next) {
    const idAutor = Number(req.params.id);
    for (const autor of autores) {
        if (autor.id === idAutor) {
            next();
            return;
        }
    }
    res.status(404).send('Autor no encontrado')
}


// --== Middleware Libros ==--
function middlewareLibros(req, res, next) {
    const idAutor = Number(req.params.id);
    const idLibro = Number(req.params.idLibro);
    for (const autor of autores) {
        if (autor.id === idAutor) {
            for (const libro of autor.libros) {
                if (libro.id === idLibro) {
                    next();
                    return;
                }
            }
        }
    }
    res.status(404).send('Libro no encontrado')
}


// --== Crear un nuevo Autor ==--
function crearAutores(req, res) {
    autores.push(req.body);
    res.status(201).send('autor cargado');
}


// --== Listar Autores ==--
function listaAutores(req, res) {
    let listaAutor = [];
    for (const autor of autores) {
        listaAutor.push(autor.nombre + " " + autor.apellido);
    }
    res.status(200).json(listaAutor);
}


// --== Listar Autores por ID ==--
function listaAutoresId(req, res) {
    const idAutor = Number(req.params.id);
    for (const autor of autores) {
        if (autor.id === idAutor) {
            res.status(200).send(autor.nombre + " " + autor.apellido);
        }
    }
}


// --== Eliminar Autor por ID ==--
function eliminarAutoreId(req, res) {
    const idAutor = Number(req.params.id);
    for (const autor of autores) {
        if (autor.id === idAutor) {
            const pos = autores.indexOf(autor);
            autores.splice(pos, 1)
            res.status(204).send('Se elimino el autor: ' + autor.nombre + " " + autor.apellido);
        }
    }
}


// --== Modificar Autor por ID ==--
function modificaAutorID(req, res) {
    const idAutor = Number(req.params.id);
    for (const autor of autores) {
        if (autor.id === idAutor) {
            const pos = autores.indexOf(autor);
            autores[pos].id = req.body.id;
            autores[pos].nombre = req.body.nombre;
            autores[pos].apellido = req.body.apellido;
            autores[pos].bornDate = req.body.bornDate;
            res.status(204).send('Se a Modificado el Autor');
        }
    }
}


// --== Listar libros de Autor por ID ==--
function listaLibrosId(req, res) {
    const idAutor = Number(req.params.id);
    for (const autor of autores) {
        if (autor.id === idAutor) {
            res.status(200).send(autor.libros);
        }
    }
}


// --== Agregar un libro por Autores por ID ==--
function agregarLibrosId(req, res) {
    const idAutor = Number(req.params.id);
    for (const autor of autores) {
        if (autor.id === idAutor) {
            const pos = autores.indexOf(autor);
            autores[pos].libros.push(req.body);
            res.status(201).send('libro cargado');
        }
    }
}


// --== Buscar libros por Autore y ID del libro ==--
function librosId(req, res) {
    const idAutor = Number(req.params.id);
    const idLibro = Number(req.params.idLibro);
    for (const autor of autores) {
        if (autor.id === idAutor) {
            for (const libro of autor.libros) {
                if (libro.id === idLibro) {
                    res.status(200).json(libro);
                }
            }
        }
    }
}


// --== Modificar libros por Autore y ID del libro ==--
function modificaLibrosId(req, res) {
    const idAutor = Number(req.params.id);
    const idLibro = Number(req.params.idLibro);
    for (const autor of autores) {
        if (autor.id === idAutor) {
            const posA = autores.indexOf(autor);
            for (const libro of autor.libros) {
                if (libro.id === idLibro) {
                    const posL = autor.libros.indexOf(libro);
                    autores[posA].libros[posL].id = req.body.id;
                    autores[posA].libros[posL].titulo = req.body.titulo;
                    autores[posA].libros[posL].descripcion = req.body.descripcion;
                    autores[posA].libros[posL].publicacion = req.body.publicacion;
                    res.status(204).send('Libro modificado');
                }
            }
        }
    }
}



// --== Eliminar libros por Autore y ID del libro ==--
function eliminarLibrosId(req, res) {
    const idAutor = Number(req.params.id);
    const idLibro = Number(req.params.idLibro);
    for (const autor of autores) {
        if (autor.id === idAutor) {
            const posA = autores.indexOf(autor);
            for (const libro of autor.libros) {
                if (libro.id === idLibro) {
                    const posL = autor.libros.indexOf(libro);
                    autores[posA].libros.splice(posL, 1);
                    res.status(204).send('Libro Eliminado');
                }
            }
        }
    }
}



module.exports = {
    listaAutores,
    crearAutores,
    listaAutoresId,
    eliminarAutoreId,
    modificaAutorID,
    listaLibrosId,
    agregarLibrosId,
    librosId,
    modificaLibrosId,
    eliminarLibrosId,
    middlewareAutores,
    middlewareLibros
}