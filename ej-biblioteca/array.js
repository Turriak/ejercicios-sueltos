let autores =
    [{
        id: 1,
        nombre: "Jorge Luis",
        apellido: "Borges",
        bornDate: "24/08/1889",
        libros: [{
            id: 1,
            titulo: "Ficciones",
            descripcion: "De terror es este",
            publicacion: 1944
        },
        {
            id: 2,
            titulo: "El Aleph",
            descripcion: "Este es de cuando pegaba la locura",
            publicacion: 1949
        }
        ]
    },
    {
        id: 2,
        nombre: "Gabriel",
        apellido: "Garcia Marquez",
        bornDate: "06/03/1927",
        libros: [{
            id: 1,
            titulo: "100 anios de soledad",
            descripcion: "De fantasia, un lujo.",
            publicacion: 1967
        },
        {
            id: 2,
            titulo: "El coronel no tiene quien le escriba",
            descripcion: "Este es de soledad a pleno..",
            publicacion: 1961
        },
        {
            id: 3,
            titulo: "La horasca",
            descripcion: "De la weed es este..",
            publicacion: 1955
        }]
    },
    {
        id: 3,
        nombre: "Dani",
        apellido: "Santos",
        bornDate: "21/05/1987",
        libros: [{
            id: 1,
            titulo: "Como se agradece",
            descripcion: "Estaba en una nota eterna..",
            publicacion: 2014
        },
        {
            id: 2,
            titulo: "Crazy and young",
            descripcion: "Un kamikaze",
            publicacion: 2016
        }]
    }];

module.exports = {
    autores
}
