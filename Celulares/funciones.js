class Cellphone {
    constructor(mar, gam, mod, pan, os, pre) {
        this.marca = mar;
        this.gama = gam;
        this.modelo = mod;
        this.pantall = pan;
        this.os = os;
        this.precio = pre;
    }
}

// --== funcion para crear un objeto celular ==--
function newCell(mar, gam, mod, pan, os, pre) {
    const phone = new Cellphone(mar, gam, mod, pan, os, pre);
    return phone;
}

// --== funcion que avisa que el servidor esta ready ==--
function serverReady() {
    console.log('the server is ready');
}

// --== funcion que muestra los objetos del array ==--
function mostrarCelus(req, res) {

    res.send(listacelu);
}

// --== funcion devuelve el celurar mas barato ==--
function menorPrecio(req, res) {
    let lower = Infinity;
    for (const celu of listacelu) {
        if (celu.precio < lower) {
            lower = celu.precio;
            celuLow = celu;
        }
    }
    // console.log(celuLow) solo pra ver por consola que devuelve
    res.send(celuLow);
}

// --== funcion devuelve el celurar mas caro ==--
function mayorPrecio(req, res) {
    let higher = 0;
    for (const celu of listacelu) {
        if (celu.precio > higher) {
            higher = celu.precio;
            celuLow = celu;
        }
    }
    // console.log(celuLow) solo pra ver por consola que devuelve
    res.send(celuLow);
}

// --== funcion devuelve por gama ==--
function mostrarCelus(req, res) {
    let gamaA =[];
    let gamaM =[];
    let gamaB =[];
    
    for (const celu of listacelu){
        switch (celu.gama) {
            case "Alta":
                gamaA.push(celu);
            break;

            case "Media":
                gamaM.push(celu);
            break;
       
            case "Baja":
                gamaB.push(celu);
            break;
        }
    } 
    res.send(`<!DOCTYPE html>
    <html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Mostra celus</title>
    </head>
    <body>
        <h1>Lista de Celulares</h1>
        <div>
            <label>Gama Alta</label><br>
            ${JSON.stringify(gamaA)}
            </div>
            <div>
            <label>Gama Media</label><br>
            ${JSON.stringify(gamaM)}
            </div>
            <div>
            <label>Gama Baja</label><br>
            ${JSON.stringify(gamaB)}
            </div>
        
    </body>
    </html`);

    
}



const listacelu = [
    newCell("Apple", "Alta", "iPhone 11 Pro Max", '5"', "OS 14", 80000),
    newCell("Samsung", "Alta", "Galaxy S21", '5.5"', "OS 15", 120000),
    newCell("Motora", "Media", "G8", '6.4"', "Andoid", 34000),
    newCell("Google", "Media", "Pixel 3", '5.5"', "Android", 60000),
    newCell("Samsung", "Baja", "Galaxy A01", '5.3"', "Andorid", 14000),
    newCell("Noblex", "Baja", "N405", '4"', "Android", 11000),
];

// console.log(listacelu);
// menorPrecio(listacelu);


module.exports = {
    serverReady,
    mostrarCelus,
    menorPrecio,
    listacelu,
    mayorPrecio,
    mostrarCelus
}