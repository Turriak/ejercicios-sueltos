const express = require('express');
const { serverReady, mostrarCelus, menorPrecio, mayorPrecio, listacelu } = require('./funciones');
const server = express();



server.get('/', mostrarCelus);
server.get('/menorprecio', menorPrecio);
server.get('/mayorprecio', mayorPrecio);

server.listen(9090, serverReady);