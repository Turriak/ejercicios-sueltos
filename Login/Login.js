
let listaUsuarios = new Array();

// Creamos la clase usuario
class Usuario {
        constructor (nom,ape,mail,pais,pass) {
            this.nombre = nom;
            this.apellido = ape;
            this.email = mail;
            this.pais = pais;
            this.contraseña = pass;
        }      
}

// --== funcion para crear el usuario le pasamos los parametros y nos retorna un objeto de la clase usuario==--
function crearUsuarios(nom,ape,mail,pais,pass) {
    newUsuario = new Usuario(nom,ape,mail,pais,pass); //creamos el objeto de la clase usuario y 
    return newUsuario;    //retorna el objeto usuario
}
// --== funcion para validar que el mail este registrado, si exite nos informa que el mail ya esta registrado y retorna un false ==--
function validarEmail(mail) {
    for (usuario of listaUsuarios){     // recorremos el array en busca del mail
        if (usuario.email === mail){
            document.querySelector('.email').focus(); // nos posiciona el cursor en el input
            alert("Ya existe ese mail registrado");
            return false;
        }
    }
        return true;    
    } 

// --== funcion para validad que la confirmación de la contraseña sea igual a la contraseña ==--    
function validarPass(pass,repass) {
    if (pass !== repass) {
        document.querySelector('.confirpass').focus(); // nos posiciona el cursor en el input
        alert("confirmación de password incorrecta");
        return false;
    }
    return true;
}

// --== funcion que valida si el usuario esta registrado y nos avisa en que posición del array esta dicho usuario==--
function login(mail,pass) {
    for (const i in listaUsuarios) {  //recorremos el array buscanco si el usuario esta registrado
        if ((listaUsuarios[i].email === mail) && (listaUsuarios[i].contraseña === pass)){
            alert('login exitoso el indice del arary es: '+ i);
            return;
        }               
    }
    alert('Usuario no registrado');
}



// --== funcion que es llamda cuando apretamos boton registra usuario ==--
function cargaUsuario() {
    const nom = document.querySelector('.nombre').value;
    const ape = document.querySelector('.apellido').value;
    const mail = document.querySelector('.email').value;
    const pais = document.querySelector('.pais').value;
    const pass = document.querySelector('.password').value;
    const repass = document.querySelector('.confirpass').value;
    if ((validarEmail(mail)) && (validarPass(pass,repass))) {      // valida el mail y contraseña llamando a las funciones de validación
        listaUsuarios.push(crearUsuarios(nom,ape,mail,pais,pass)); // si valida agrega al array un usuario nuevo con la funcion crearUsuario
        document.querySelector('.nombre').value ="";
        document.querySelector('.apellido').value ="";
        document.querySelector('.email').value ="";
        document.querySelector('.pais').value ="";
        document.querySelector('.password').value ="";
        document.querySelector('.confirpass').value ="";
    }
}


// --== funcion que es llamada cuando apretamos boton logear usuario ==--
function loginUser() {
    const mail = document.querySelector('.em').value;
    const pass = document.querySelector('.pass').value;
    if ((mail !=="") && (pass !=="")){                      // valida que los campos no esten vacios 
        document.querySelector('.em').value ="";
        document.querySelector('.pass').value ="";
        login(mail,pass);     // llama a la funcion login, la cual valida si esta registrado el usuario
    } else {
        document.querySelector('.em').focus();
        alert("ingrese e-mail y contraseña");
    }
}




